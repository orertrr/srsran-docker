#!/bin/sh

# Base Image
sudo docker build -t srsran:latest \
    --build-arg SRSRAN_PATH=PATH \
    --build-arg MAKE_JOBS=4 \
    .