#!/bin/sh

set -e

# Network Namespace Creation
ip netns add ue1

exec "$@"