#!/bin/sh

sudo docker run -it --rm \
    --name srsue \
    --entrypoint "./ue_entrypoint.sh" \
    --network "srsran" \
    --cap-add CAP_SYS_ADMIN \
    --cap-add NET_ADMIN \
    --device /dev/net/tun \
    --security-opt apparmor=unconfined \
    --ip 172.18.0.4 \
    -v $(pwd)/config/:/root/.config/srsran/ \
    -v $(pwd)/log/:/tmp/ \
    srsran:latest srsue