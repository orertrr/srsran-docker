# srsRAN-docker

可以先調整 build.sh 內的參數
- SRSRAN_PATH : srsRAN source code 路徑

建立 image
```
./build.sh
```

執行 epc, enb, ue
```sh
# 使用 docker compose
docker compose up epc
docker compose up enb
docker compose up ue

# 使用 shell script
docker network create \
    --driver bridge \
    --subnet 172.18.0.0/16 \
    --gateway 172.18.0.254 \
    srsran

./run_epc.sh
./run_enb.sh
./run_ue.sh
```