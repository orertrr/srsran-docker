#!/bin/sh

sudo docker run -it --rm \
    --name srsenb \
    --network "srsran" --ip 172.18.0.3 \
    --privileged \
    --ipc "shareable" \
    -v $(pwd)/config/:/root/.config/srsran/ \
    -v $(pwd)/log/:/tmp/ \
    srsran:latest srsenb