FROM ubuntu:latest

# Args
ARG SRSRAN_PATH
ARG MAKE_JOBS=1

WORKDIR /srsran

# Copy source code
COPY ${SRSRAN_PATH} /srsran

COPY ./ue_entrypoint.sh /srsran
RUN chmod +x /srsran/ue_entrypoint.sh

# Install dependencies
RUN apt update
RUN apt install -y  \
        iproute2 \
        build-essential \
        cmake libfftw3-dev \
        libmbedtls-dev \
        libboost-program-options-dev \
        libconfig++-dev \
        libsctp-dev \
        libzmq3-dev

# Compile and install srsRAN
RUN mkdir /srsran/build && cd build && \
    cmake .. && \
    make -j ${MAKE_JOBS} && \
#   make -j ${MAKE_JOBS} test && \
    make install && \
    ldconfig