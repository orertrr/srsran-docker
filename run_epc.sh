#!/bin/sh

sudo docker run -it --rm \
    --name srsepc \
    --network "srsran" --ip 172.18.0.2 \
    --cap-add NET_ADMIN \
    --cap-add SYS_NICE \
    --device /dev/net/tun \
    -v $(pwd)/config/:/root/.config/srsran/ \
    -v $(pwd)/log/:/tmp/ \
    srsran:latest srsepc